/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03testox;

/**
 *
 * @author asus
 */
class OXTestbyme {

    static boolean checkWinner(char[][] board, char currentPlayer) {
        if (checkRow(board, currentPlayer) || checkCol(board, currentPlayer) || checkX1(board, currentPlayer) || checkX2(board, currentPlayer) || checkDraw(board, currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(char[][] board, char currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(board, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(char[][] board, char currentPlayer, int row) {
        for (int i = 0; i < 3; i++) {
            if (board[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] board, char currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(board, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] board, char currentPlayer, int col) {
        for (int i = 0; i < 3; i++) {
            if (board[i][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[i][3 - i - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
