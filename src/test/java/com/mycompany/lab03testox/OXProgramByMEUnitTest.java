/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03testox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author asus
 */
public class OXProgramByMEUnitTest {

    public OXProgramByMEUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinnerNoPlayBY_X_output_false() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    @Test
    public void testCheckWinnerNoPlayBY_O_output_false() {
        char[][] board = {{'x', 'o', 'o'}, {'-', 'x', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    @Test
    public void testCheckWinnerPlayBY_X_output_true() {
        char[][] board = {{'x', 'o', 'o'}, {'x', 'x', 'x'}, {'-', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    @Test
    public void testCheckWinnerPlayBY_O_output_false() {
        char[][] board = {{'o', 'o', 'o'}, {'-', 'x', 'x'}, {'-', '-', '-'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    // ไม่ชนะและชนะ x แนวตั้งแถว1
    @Test
    public void testCheckWinnerCol1BY_X_output_false() {
        char[][] board = {{'x', 'o', 'o'}, {'-', 'x', 'x'}, {'-', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    @Test
    public void testCheckWinnerCol1BY_X_output_true() {
        char[][] board = {{'x', 'o', 'o'}, {'x', '-', '-'}, {'x', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    // ชนะ x แนวตั้งแถว2
    @Test
    public void testCheckWinnerCol2BY_X_output_true() {
        char[][] board = {{'o', 'x', 'o'}, {'o', 'x', '-'}, {'o', 'x', '-'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    // ชนะ x แนวตั้งแถว3
    @Test
    public void testCheckWinnerCol3BY_X_output_true() {
        char[][] board = {{'o', '-', 'x'}, {'-', '-', 'x'}, {'o', 'x', 'x'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    // ชนะ o แนวตั้งแถว1
    @Test
    public void testCheckWinnerCol1BY_O_output_true() {
        char[][] board = {{'o', '-', 'x'}, {'o', '-', 'x'}, {'o', 'x', 'x'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    // ชนะ o แนวตั้งแถว2
    @Test
    public void testCheckWinnerCol2BY_O_output_true() {
        char[][] board = {{'-', 'o', 'x'}, {'o', 'o', 'x'}, {'-', 'o', '-'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    // ชนะ o แนวตั้งแถว3
    @Test
    public void testCheckWinnerCol3BY_O_output_true() {
        char[][] board = {{'-', '-', 'o'}, {'x', '-', 'o'}, {'-', 'x', 'o'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //////
    //ชนะ x แนวนอนแถว1
    @Test
    public void testCheckWinnerRow1BY_X_output_true() {
        char[][] board = {{'x', 'x', 'x'}, {'-', '-', '-'}, {'-', 'x', 'o'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //ชนะ x แนวนอนแถว2
    @Test
    public void testCheckWinnerRow2BY_X_output_true() {
        char[][] board = {{'-', '-', '-'}, {'x', 'x', 'x'}, {'-', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //ชนะ x แนวนอนแถว3
    @Test
    public void testCheckWinnerRow3BY_X_output_true() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'x', 'x', 'x'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //ชนะ o แนวนอนแถว1
    @Test
    public void testCheckWinnerRow1BY_O_output_true() {
        char[][] board = {{'o', 'o', 'o'}, {'-', '-', '-'}, {'-', 'x', 'o'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //ชนะ o แนวนอนแถว2
    @Test
    public void testCheckWinnerRow2BY_O_output_true() {
        char[][] board = {{'-', '-', '-'}, {'o', 'o', 'o'}, {'-', '-', '-'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //ชนะ o แนวนอนแถว3
    @Test
    public void testCheckWinnerRow3BY_O_output_true() {
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'o', 'o', 'o'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //x แนวทแยงซ้าย
    //แบบไม่ชนะ
    @Test
    public void testCheckWinnerX1BY_X_output_false() {
        char[][] board = {{'x', '-', '-'}, {'-', 'o', '-'}, {'o', 'o', 'x'}};
        char currentPlayer = 'x';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //แบบชนะ
    @Test
    public void testCheckWinnerX1BY_X_output_true() {
        char[][] board = {{'x', '-', '-'}, {'-', 'x', '-'}, {'o', 'o', 'x'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //o แนวทแยงซ้าย
    //แบบไม่ชนะ
    @Test
    public void testCheckWinnerX1BY_O_output_false() {
        char[][] board = {{'x', '-', '-'}, {'-', 'o', '-'}, {'o', 'o', 'x'}};
        char currentPlayer = 'o';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //แบบชนะ
    @Test
    public void testCheckWinnerX1BY_O_output_true() {
        char[][] board = {{'o', '-', '-'}, {'-', 'o', '-'}, {'-', 'x', 'o'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }
    
    //x แนวทแยงขวา
    //แบบไม่ชนะ
    @Test
    public void testCheckWinnerX2BY_X_output_false() {
        char[][] board = {{'-', '-', 'x'}, {'-', 'x', '-'}, {'o', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //แบบชนะ
    @Test
    public void testCheckWinnerX2BY_X_output_true() {
        char[][] board = {{'-', '-', 'x'}, {'-', 'x', '-'}, {'x', '-', '-'}};
        char currentPlayer = 'x';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //o แนวทแยงขวา
    //แบบไม่ชนะ
    @Test
    public void testCheckWinnerX2BY_O_output_false() {
        char[][] board = {{'-', '-', 'o'}, {'-', 'x', '-'}, {'o', '-', '-'}};
        char currentPlayer = 'o';
        assertEquals(false, OXTestbyme.checkWinner(board, currentPlayer));
    }

    //แบบชนะ
    @Test
    public void testCheckWinnerX2BY_O_output_true() {
        char[][] board = {{'-', '-', 'o'}, {'-', 'o', '-'}, {'o', '-', '-'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }
    
    //เสมอ แบบ1
    @Test
    public void testCheckWinnerDraw1_output_true() {
        char[][] board = {{'x', 'x', 'o'}, {'o', 'o', 'x'}, {'x', 'o', 'x'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }
   
    
     //เสมอ แบบ2
    @Test
    public void testCheckWinnerDraw2_output_true() {
        char[][] board = {{'x', 'o', 'x'}, {'x', 'x', 'o'}, {'o', 'x', 'o'}};
        char currentPlayer = 'o';
        assertEquals(true, OXTestbyme.checkWinner(board, currentPlayer));
    }


}
